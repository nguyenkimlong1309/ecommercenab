package co.longuyen.store.custact;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("co.longuyen.store.custact");

        noClasses()
            .that()
                .resideInAnyPackage("co.longuyen.store.custact.service..")
            .or()
                .resideInAnyPackage("co.longuyen.store.custact.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..co.longuyen.store.custact.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}

package co.longuyen.store.custact.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CustActMapperTest {

    private CustActMapper custActMapper;

    @BeforeEach
    public void setUp() {
        custActMapper = new CustActMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        String id = "id1";
        assertThat(custActMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(custActMapper.fromId(null)).isNull();
    }
}

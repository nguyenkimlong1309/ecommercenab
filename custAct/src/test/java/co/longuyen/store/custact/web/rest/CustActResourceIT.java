package co.longuyen.store.custact.web.rest;

import co.longuyen.store.custact.CustActApp;
import co.longuyen.store.custact.domain.CustAct;
import co.longuyen.store.custact.repository.CustActRepository;
import co.longuyen.store.custact.repository.search.CustActSearchRepository;
import co.longuyen.store.custact.service.CustActService;
import co.longuyen.store.custact.service.dto.CustActDTO;
import co.longuyen.store.custact.service.mapper.CustActMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CustActResource} REST controller.
 */
@SpringBootTest(classes = CustActApp.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class CustActResourceIT {

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_DETAILS = "AAAAAAAAAA";
    private static final String UPDATED_DETAILS = "BBBBBBBBBB";

    private static final Instant DEFAULT_SENT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_SENT_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_SEARCH_COUNT = 1;
    private static final Integer UPDATED_SEARCH_COUNT = 2;

    private static final Integer DEFAULT_VIEW_COUNT = 1;
    private static final Integer UPDATED_VIEW_COUNT = 2;

    private static final Integer DEFAULT_FILTER_COUNT = 1;
    private static final Integer UPDATED_FILTER_COUNT = 2;

    private static final Long DEFAULT_CAR_ID = 1L;
    private static final Long UPDATED_CAR_ID = 2L;

    private static final String DEFAULT_IP_ADD = "AAAAAAAAAA";
    private static final String UPDATED_IP_ADD = "BBBBBBBBBB";

    @Autowired
    private CustActRepository custActRepository;

    @Autowired
    private CustActMapper custActMapper;

    @Autowired
    private CustActService custActService;

    /**
     * This repository is mocked in the co.longuyen.store.custact.repository.search test package.
     *
     * @see co.longuyen.store.custact.repository.search.CustActSearchRepositoryMockConfiguration
     */
    @Autowired
    private CustActSearchRepository mockCustActSearchRepository;

    @Autowired
    private MockMvc restCustActMockMvc;

    private CustAct custAct;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustAct createEntity() {
        CustAct custAct = new CustAct()
            .date(DEFAULT_DATE)
            .details(DEFAULT_DETAILS)
            .sentDate(DEFAULT_SENT_DATE)
            .searchCount(DEFAULT_SEARCH_COUNT)
            .viewCount(DEFAULT_VIEW_COUNT)
            .filterCount(DEFAULT_FILTER_COUNT)
            .carId(DEFAULT_CAR_ID)
            .ipAdd(DEFAULT_IP_ADD);
        return custAct;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustAct createUpdatedEntity() {
        CustAct custAct = new CustAct()
            .date(UPDATED_DATE)
            .details(UPDATED_DETAILS)
            .sentDate(UPDATED_SENT_DATE)
            .searchCount(UPDATED_SEARCH_COUNT)
            .viewCount(UPDATED_VIEW_COUNT)
            .filterCount(UPDATED_FILTER_COUNT)
            .carId(UPDATED_CAR_ID)
            .ipAdd(UPDATED_IP_ADD);
        return custAct;
    }

    @BeforeEach
    public void initTest() {
        custActRepository.deleteAll();
        custAct = createEntity();
    }

    @Test
    public void createCustAct() throws Exception {
        int databaseSizeBeforeCreate = custActRepository.findAll().size();

        // Create the CustAct
        CustActDTO custActDTO = custActMapper.toDto(custAct);
        restCustActMockMvc.perform(post("/api/cust-acts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(custActDTO)))
            .andExpect(status().isCreated());

        // Validate the CustAct in the database
        List<CustAct> custActList = custActRepository.findAll();
        assertThat(custActList).hasSize(databaseSizeBeforeCreate + 1);
        CustAct testCustAct = custActList.get(custActList.size() - 1);
        assertThat(testCustAct.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testCustAct.getDetails()).isEqualTo(DEFAULT_DETAILS);
        assertThat(testCustAct.getSentDate()).isEqualTo(DEFAULT_SENT_DATE);
        assertThat(testCustAct.getSearchCount()).isEqualTo(DEFAULT_SEARCH_COUNT);
        assertThat(testCustAct.getViewCount()).isEqualTo(DEFAULT_VIEW_COUNT);
        assertThat(testCustAct.getFilterCount()).isEqualTo(DEFAULT_FILTER_COUNT);
        assertThat(testCustAct.getCarId()).isEqualTo(DEFAULT_CAR_ID);
        assertThat(testCustAct.getIpAdd()).isEqualTo(DEFAULT_IP_ADD);

        // Validate the CustAct in Elasticsearch
        verify(mockCustActSearchRepository, times(1)).save(testCustAct);
    }

    @Test
    public void createCustActWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = custActRepository.findAll().size();

        // Create the CustAct with an existing ID
        custAct.setId("existing_id");
        CustActDTO custActDTO = custActMapper.toDto(custAct);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustActMockMvc.perform(post("/api/cust-acts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(custActDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CustAct in the database
        List<CustAct> custActList = custActRepository.findAll();
        assertThat(custActList).hasSize(databaseSizeBeforeCreate);

        // Validate the CustAct in Elasticsearch
        verify(mockCustActSearchRepository, times(0)).save(custAct);
    }


    @Test
    public void checkDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = custActRepository.findAll().size();
        // set the field null
        custAct.setDate(null);

        // Create the CustAct, which fails.
        CustActDTO custActDTO = custActMapper.toDto(custAct);

        restCustActMockMvc.perform(post("/api/cust-acts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(custActDTO)))
            .andExpect(status().isBadRequest());

        List<CustAct> custActList = custActRepository.findAll();
        assertThat(custActList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkSentDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = custActRepository.findAll().size();
        // set the field null
        custAct.setSentDate(null);

        // Create the CustAct, which fails.
        CustActDTO custActDTO = custActMapper.toDto(custAct);

        restCustActMockMvc.perform(post("/api/cust-acts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(custActDTO)))
            .andExpect(status().isBadRequest());

        List<CustAct> custActList = custActRepository.findAll();
        assertThat(custActList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkCarIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = custActRepository.findAll().size();
        // set the field null
        custAct.setCarId(null);

        // Create the CustAct, which fails.
        CustActDTO custActDTO = custActMapper.toDto(custAct);

        restCustActMockMvc.perform(post("/api/cust-acts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(custActDTO)))
            .andExpect(status().isBadRequest());

        List<CustAct> custActList = custActRepository.findAll();
        assertThat(custActList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllCustActs() throws Exception {
        // Initialize the database
        custActRepository.save(custAct);

        // Get all the custActList
        restCustActMockMvc.perform(get("/api/cust-acts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(custAct.getId())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].details").value(hasItem(DEFAULT_DETAILS)))
            .andExpect(jsonPath("$.[*].sentDate").value(hasItem(DEFAULT_SENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].searchCount").value(hasItem(DEFAULT_SEARCH_COUNT)))
            .andExpect(jsonPath("$.[*].viewCount").value(hasItem(DEFAULT_VIEW_COUNT)))
            .andExpect(jsonPath("$.[*].filterCount").value(hasItem(DEFAULT_FILTER_COUNT)))
            .andExpect(jsonPath("$.[*].carId").value(hasItem(DEFAULT_CAR_ID.intValue())))
            .andExpect(jsonPath("$.[*].ipAdd").value(hasItem(DEFAULT_IP_ADD)));
    }
    
    @Test
    public void getCustAct() throws Exception {
        // Initialize the database
        custActRepository.save(custAct);

        // Get the custAct
        restCustActMockMvc.perform(get("/api/cust-acts/{id}", custAct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(custAct.getId()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.details").value(DEFAULT_DETAILS))
            .andExpect(jsonPath("$.sentDate").value(DEFAULT_SENT_DATE.toString()))
            .andExpect(jsonPath("$.searchCount").value(DEFAULT_SEARCH_COUNT))
            .andExpect(jsonPath("$.viewCount").value(DEFAULT_VIEW_COUNT))
            .andExpect(jsonPath("$.filterCount").value(DEFAULT_FILTER_COUNT))
            .andExpect(jsonPath("$.carId").value(DEFAULT_CAR_ID.intValue()))
            .andExpect(jsonPath("$.ipAdd").value(DEFAULT_IP_ADD));
    }

    @Test
    public void getNonExistingCustAct() throws Exception {
        // Get the custAct
        restCustActMockMvc.perform(get("/api/cust-acts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCustAct() throws Exception {
        // Initialize the database
        custActRepository.save(custAct);

        int databaseSizeBeforeUpdate = custActRepository.findAll().size();

        // Update the custAct
        CustAct updatedCustAct = custActRepository.findById(custAct.getId()).get();
        updatedCustAct
            .date(UPDATED_DATE)
            .details(UPDATED_DETAILS)
            .sentDate(UPDATED_SENT_DATE)
            .searchCount(UPDATED_SEARCH_COUNT)
            .viewCount(UPDATED_VIEW_COUNT)
            .filterCount(UPDATED_FILTER_COUNT)
            .carId(UPDATED_CAR_ID)
            .ipAdd(UPDATED_IP_ADD);
        CustActDTO custActDTO = custActMapper.toDto(updatedCustAct);

        restCustActMockMvc.perform(put("/api/cust-acts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(custActDTO)))
            .andExpect(status().isOk());

        // Validate the CustAct in the database
        List<CustAct> custActList = custActRepository.findAll();
        assertThat(custActList).hasSize(databaseSizeBeforeUpdate);
        CustAct testCustAct = custActList.get(custActList.size() - 1);
        assertThat(testCustAct.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testCustAct.getDetails()).isEqualTo(UPDATED_DETAILS);
        assertThat(testCustAct.getSentDate()).isEqualTo(UPDATED_SENT_DATE);
        assertThat(testCustAct.getSearchCount()).isEqualTo(UPDATED_SEARCH_COUNT);
        assertThat(testCustAct.getViewCount()).isEqualTo(UPDATED_VIEW_COUNT);
        assertThat(testCustAct.getFilterCount()).isEqualTo(UPDATED_FILTER_COUNT);
        assertThat(testCustAct.getCarId()).isEqualTo(UPDATED_CAR_ID);
        assertThat(testCustAct.getIpAdd()).isEqualTo(UPDATED_IP_ADD);

        // Validate the CustAct in Elasticsearch
        verify(mockCustActSearchRepository, times(1)).save(testCustAct);
    }

    @Test
    public void updateNonExistingCustAct() throws Exception {
        int databaseSizeBeforeUpdate = custActRepository.findAll().size();

        // Create the CustAct
        CustActDTO custActDTO = custActMapper.toDto(custAct);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCustActMockMvc.perform(put("/api/cust-acts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(custActDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CustAct in the database
        List<CustAct> custActList = custActRepository.findAll();
        assertThat(custActList).hasSize(databaseSizeBeforeUpdate);

        // Validate the CustAct in Elasticsearch
        verify(mockCustActSearchRepository, times(0)).save(custAct);
    }

    @Test
    public void deleteCustAct() throws Exception {
        // Initialize the database
        custActRepository.save(custAct);

        int databaseSizeBeforeDelete = custActRepository.findAll().size();

        // Delete the custAct
        restCustActMockMvc.perform(delete("/api/cust-acts/{id}", custAct.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CustAct> custActList = custActRepository.findAll();
        assertThat(custActList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the CustAct in Elasticsearch
        verify(mockCustActSearchRepository, times(1)).deleteById(custAct.getId());
    }

    @Test
    public void searchCustAct() throws Exception {
        // Initialize the database
        custActRepository.save(custAct);
        when(mockCustActSearchRepository.search(queryStringQuery("id:" + custAct.getId())))
            .thenReturn(Collections.singletonList(custAct));
        // Search the custAct
        restCustActMockMvc.perform(get("/api/_search/cust-acts?query=id:" + custAct.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(custAct.getId())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].details").value(hasItem(DEFAULT_DETAILS)))
            .andExpect(jsonPath("$.[*].sentDate").value(hasItem(DEFAULT_SENT_DATE.toString())))
            .andExpect(jsonPath("$.[*].searchCount").value(hasItem(DEFAULT_SEARCH_COUNT)))
            .andExpect(jsonPath("$.[*].viewCount").value(hasItem(DEFAULT_VIEW_COUNT)))
            .andExpect(jsonPath("$.[*].filterCount").value(hasItem(DEFAULT_FILTER_COUNT)))
            .andExpect(jsonPath("$.[*].carId").value(hasItem(DEFAULT_CAR_ID.intValue())))
            .andExpect(jsonPath("$.[*].ipAdd").value(hasItem(DEFAULT_IP_ADD)));
    }
}

package co.longuyen.store.custact.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import co.longuyen.store.custact.web.rest.TestUtil;

public class CustActTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustAct.class);
        CustAct custAct1 = new CustAct();
        custAct1.setId("id1");
        CustAct custAct2 = new CustAct();
        custAct2.setId(custAct1.getId());
        assertThat(custAct1).isEqualTo(custAct2);
        custAct2.setId("id2");
        assertThat(custAct1).isNotEqualTo(custAct2);
        custAct1.setId(null);
        assertThat(custAct1).isNotEqualTo(custAct2);
    }
}

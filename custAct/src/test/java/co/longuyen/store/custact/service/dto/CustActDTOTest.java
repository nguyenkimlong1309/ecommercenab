package co.longuyen.store.custact.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import co.longuyen.store.custact.web.rest.TestUtil;

public class CustActDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustActDTO.class);
        CustActDTO custActDTO1 = new CustActDTO();
        custActDTO1.setId("id1");
        CustActDTO custActDTO2 = new CustActDTO();
        assertThat(custActDTO1).isNotEqualTo(custActDTO2);
        custActDTO2.setId(custActDTO1.getId());
        assertThat(custActDTO1).isEqualTo(custActDTO2);
        custActDTO2.setId("id2");
        assertThat(custActDTO1).isNotEqualTo(custActDTO2);
        custActDTO1.setId(null);
        assertThat(custActDTO1).isNotEqualTo(custActDTO2);
    }
}

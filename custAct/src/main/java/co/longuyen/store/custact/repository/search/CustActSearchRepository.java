package co.longuyen.store.custact.repository.search;

import co.longuyen.store.custact.domain.CustAct;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link CustAct} entity.
 */
public interface CustActSearchRepository extends ElasticsearchRepository<CustAct, String> {
}

package co.longuyen.store.custact.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import javax.validation.constraints.*;

import org.springframework.data.elasticsearch.annotations.FieldType;
import java.io.Serializable;
import java.util.Objects;
import java.time.Instant;

/**
 * A CustAct.
 */
@Document(collection = "cust_act")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "custact")
public class CustAct implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Field("date")
    private Instant date;

    @Field("details")
    private String details;

    @NotNull
    @Field("sent_date")
    private Instant sentDate;

    @Field("search_count")
    private Integer searchCount;

    @Field("view_count")
    private Integer viewCount;

    @Field("filter_count")
    private Integer filterCount;

    @NotNull
    @Field("car_id")
    private Long carId;

    @Field("ip_add")
    private String ipAdd;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public CustAct date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getDetails() {
        return details;
    }

    public CustAct details(String details) {
        this.details = details;
        return this;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Instant getSentDate() {
        return sentDate;
    }

    public CustAct sentDate(Instant sentDate) {
        this.sentDate = sentDate;
        return this;
    }

    public void setSentDate(Instant sentDate) {
        this.sentDate = sentDate;
    }

    public Integer getSearchCount() {
        return searchCount;
    }

    public CustAct searchCount(Integer searchCount) {
        this.searchCount = searchCount;
        return this;
    }

    public void setSearchCount(Integer searchCount) {
        this.searchCount = searchCount;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public CustAct viewCount(Integer viewCount) {
        this.viewCount = viewCount;
        return this;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getFilterCount() {
        return filterCount;
    }

    public CustAct filterCount(Integer filterCount) {
        this.filterCount = filterCount;
        return this;
    }

    public void setFilterCount(Integer filterCount) {
        this.filterCount = filterCount;
    }

    public Long getCarId() {
        return carId;
    }

    public CustAct carId(Long carId) {
        this.carId = carId;
        return this;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getIpAdd() {
        return ipAdd;
    }

    public CustAct ipAdd(String ipAdd) {
        this.ipAdd = ipAdd;
        return this;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CustAct)) {
            return false;
        }
        return id != null && id.equals(((CustAct) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "CustAct{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", details='" + getDetails() + "'" +
            ", sentDate='" + getSentDate() + "'" +
            ", searchCount=" + getSearchCount() +
            ", viewCount=" + getViewCount() +
            ", filterCount=" + getFilterCount() +
            ", carId=" + getCarId() +
            ", ipAdd='" + getIpAdd() + "'" +
            "}";
    }
}

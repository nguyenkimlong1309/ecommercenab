package co.longuyen.store.custact.repository;

import co.longuyen.store.custact.domain.CustAct;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Spring Data MongoDB repository for the CustAct entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustActRepository extends MongoRepository<CustAct, String> {
}

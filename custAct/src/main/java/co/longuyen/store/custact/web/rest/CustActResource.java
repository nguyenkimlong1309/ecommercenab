package co.longuyen.store.custact.web.rest;

import co.longuyen.store.custact.service.CustActService;
import co.longuyen.store.custact.web.rest.errors.BadRequestAlertException;
import co.longuyen.store.custact.service.dto.CustActDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link co.longuyen.store.custact.domain.CustAct}.
 */
@RestController
@RequestMapping("/api")
public class CustActResource {

    private final Logger log = LoggerFactory.getLogger(CustActResource.class);

    private static final String ENTITY_NAME = "custActCustAct";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CustActService custActService;

    public CustActResource(CustActService custActService) {
        this.custActService = custActService;
    }

    /**
     * {@code POST  /cust-acts} : Create a new custAct.
     *
     * @param custActDTO the custActDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new custActDTO, or with status {@code 400 (Bad Request)} if the custAct has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cust-acts")
    public ResponseEntity<CustActDTO> createCustAct(@Valid @RequestBody CustActDTO custActDTO) throws URISyntaxException {
        log.debug("REST request to save CustAct : {}", custActDTO);
        if (custActDTO.getId() != null) {
            throw new BadRequestAlertException("A new custAct cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustActDTO result = custActService.save(custActDTO);
        return ResponseEntity.created(new URI("/api/cust-acts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cust-acts} : Updates an existing custAct.
     *
     * @param custActDTO the custActDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated custActDTO,
     * or with status {@code 400 (Bad Request)} if the custActDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the custActDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cust-acts")
    public ResponseEntity<CustActDTO> updateCustAct(@Valid @RequestBody CustActDTO custActDTO) throws URISyntaxException {
        log.debug("REST request to update CustAct : {}", custActDTO);
        if (custActDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CustActDTO result = custActService.save(custActDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, custActDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cust-acts} : get all the custActs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of custActs in body.
     */
    @GetMapping("/cust-acts")
    public List<CustActDTO> getAllCustActs() {
        log.debug("REST request to get all CustActs");
        return custActService.findAll();
    }

    /**
     * {@code GET  /cust-acts/:id} : get the "id" custAct.
     *
     * @param id the id of the custActDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the custActDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cust-acts/{id}")
    public ResponseEntity<CustActDTO> getCustAct(@PathVariable String id) {
        log.debug("REST request to get CustAct : {}", id);
        Optional<CustActDTO> custActDTO = custActService.findOne(id);
        return ResponseUtil.wrapOrNotFound(custActDTO);
    }

    /**
     * {@code DELETE  /cust-acts/:id} : delete the "id" custAct.
     *
     * @param id the id of the custActDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cust-acts/{id}")
    public ResponseEntity<Void> deleteCustAct(@PathVariable String id) {
        log.debug("REST request to delete CustAct : {}", id);
        custActService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }

    /**
     * {@code SEARCH  /_search/cust-acts?query=:query} : search for the custAct corresponding
     * to the query.
     *
     * @param query the query of the custAct search.
     * @return the result of the search.
     */
    @GetMapping("/_search/cust-acts")
    public List<CustActDTO> searchCustActs(@RequestParam String query) {
        log.debug("REST request to search CustActs for query {}", query);
        return custActService.search(query);
    }
}

/**
 * View Models used by Spring MVC REST controllers.
 */
package co.longuyen.store.custact.web.rest.vm;

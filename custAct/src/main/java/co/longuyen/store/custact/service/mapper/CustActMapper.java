package co.longuyen.store.custact.service.mapper;


import co.longuyen.store.custact.domain.*;
import co.longuyen.store.custact.service.dto.CustActDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CustAct} and its DTO {@link CustActDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CustActMapper extends EntityMapper<CustActDTO, CustAct> {



    default CustAct fromId(String id) {
        if (id == null) {
            return null;
        }
        CustAct custAct = new CustAct();
        custAct.setId(id);
        return custAct;
    }
}

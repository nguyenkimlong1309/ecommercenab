package co.longuyen.store.custact.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link co.longuyen.store.custact.domain.CustAct} entity.
 */
public class CustActDTO implements Serializable {
    
    private String id;

    @NotNull
    private Instant date;

    private String details;

    @NotNull
    private Instant sentDate;

    private Integer searchCount;

    private Integer viewCount;

    private Integer filterCount;

    @NotNull
    private Long carId;

    private String ipAdd;

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public Instant getSentDate() {
        return sentDate;
    }

    public void setSentDate(Instant sentDate) {
        this.sentDate = sentDate;
    }

    public Integer getSearchCount() {
        return searchCount;
    }

    public void setSearchCount(Integer searchCount) {
        this.searchCount = searchCount;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getFilterCount() {
        return filterCount;
    }

    public void setFilterCount(Integer filterCount) {
        this.filterCount = filterCount;
    }

    public Long getCarId() {
        return carId;
    }

    public void setCarId(Long carId) {
        this.carId = carId;
    }

    public String getIpAdd() {
        return ipAdd;
    }

    public void setIpAdd(String ipAdd) {
        this.ipAdd = ipAdd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustActDTO custActDTO = (CustActDTO) o;
        if (custActDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), custActDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CustActDTO{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", details='" + getDetails() + "'" +
            ", sentDate='" + getSentDate() + "'" +
            ", searchCount=" + getSearchCount() +
            ", viewCount=" + getViewCount() +
            ", filterCount=" + getFilterCount() +
            ", carId=" + getCarId() +
            ", ipAdd='" + getIpAdd() + "'" +
            "}";
    }
}

package co.longuyen.store.custact.service;

import co.longuyen.store.custact.domain.CustAct;
import co.longuyen.store.custact.repository.CustActRepository;
import co.longuyen.store.custact.repository.search.CustActSearchRepository;
import co.longuyen.store.custact.service.dto.CustActDTO;
import co.longuyen.store.custact.service.mapper.CustActMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link CustAct}.
 */
@Service
public class CustActService {

    private final Logger log = LoggerFactory.getLogger(CustActService.class);

    private final CustActRepository custActRepository;

    private final CustActMapper custActMapper;

    private final CustActSearchRepository custActSearchRepository;

    public CustActService(CustActRepository custActRepository, CustActMapper custActMapper, CustActSearchRepository custActSearchRepository) {
        this.custActRepository = custActRepository;
        this.custActMapper = custActMapper;
        this.custActSearchRepository = custActSearchRepository;
    }

    /**
     * Save a custAct.
     *
     * @param custActDTO the entity to save.
     * @return the persisted entity.
     */
    public CustActDTO save(CustActDTO custActDTO) {
        log.debug("Request to save CustAct : {}", custActDTO);
        CustAct custAct = custActMapper.toEntity(custActDTO);
        custAct = custActRepository.save(custAct);
        CustActDTO result = custActMapper.toDto(custAct);
        custActSearchRepository.save(custAct);
        return result;
    }

    /**
     * Get all the custActs.
     *
     * @return the list of entities.
     */
    public List<CustActDTO> findAll() {
        log.debug("Request to get all CustActs");
        return custActRepository.findAll().stream()
            .map(custActMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one custAct by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Optional<CustActDTO> findOne(String id) {
        log.debug("Request to get CustAct : {}", id);
        return custActRepository.findById(id)
            .map(custActMapper::toDto);
    }

    /**
     * Delete the custAct by id.
     *
     * @param id the id of the entity.
     */
    public void delete(String id) {
        log.debug("Request to delete CustAct : {}", id);
        custActRepository.deleteById(id);
        custActSearchRepository.deleteById(id);
    }

    /**
     * Search for the custAct corresponding to the query.
     *
     * @param query the query of the search.
     * @return the list of entities.
     */
    public List<CustActDTO> search(String query) {
        log.debug("Request to search CustActs for query {}", query);
        return StreamSupport
            .stream(custActSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .map(custActMapper::toDto)
            .collect(Collectors.toList());
    }
}

import { Moment } from 'moment';

export interface ICustAct {
  id?: number;
  date?: Moment;
  details?: string;
  sentDate?: Moment;
  searchCount?: number;
  viewCount?: number;
  filterCount?: number;
  carId?: number;
  ipAdd?: string;
}

export class CustAct implements ICustAct {
  constructor(
    public id?: number,
    public date?: Moment,
    public details?: string,
    public sentDate?: Moment,
    public searchCount?: number,
    public viewCount?: number,
    public filterCount?: number,
    public carId?: number,
    public ipAdd?: string
  ) {}
}

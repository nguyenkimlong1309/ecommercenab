export const enum Color {
  RED = 'RED',
  WHITE = 'WHITE',
  YELLOW = 'YELLOW',
  BLACK = 'BLACK',
  GREEN = 'GREEN'
}

import { ICar } from 'app/shared/model/car/car.model';

export interface ICarCategory {
  id?: number;
  name?: string;
  description?: string;
  cars?: ICar[];
}

export class CarCategory implements ICarCategory {
  constructor(public id?: number, public name?: string, public description?: string, public cars?: ICar[]) {}
}

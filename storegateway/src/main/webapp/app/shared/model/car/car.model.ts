import { Color } from 'app/shared/model/enumerations/color.model';

export interface ICar {
  id?: number;
  name?: string;
  description?: string;
  price?: number;
  branch?: string;
  color?: Color;
  imageContentType?: string;
  image?: any;
  carCategoryName?: string;
  carCategoryId?: number;
}

export class Car implements ICar {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public price?: number,
    public branch?: string,
    public color?: Color,
    public imageContentType?: string,
    public image?: any,
    public carCategoryName?: string,
    public carCategoryId?: number
  ) {}
}

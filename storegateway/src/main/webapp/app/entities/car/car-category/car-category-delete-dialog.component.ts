import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICarCategory } from 'app/shared/model/car/car-category.model';
import { CarCategoryService } from './car-category.service';

@Component({
  templateUrl: './car-category-delete-dialog.component.html'
})
export class CarCategoryDeleteDialogComponent {
  carCategory?: ICarCategory;

  constructor(
    protected carCategoryService: CarCategoryService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.carCategoryService.delete(id).subscribe(() => {
      this.eventManager.broadcast('carCategoryListModification');
      this.activeModal.close();
    });
  }
}

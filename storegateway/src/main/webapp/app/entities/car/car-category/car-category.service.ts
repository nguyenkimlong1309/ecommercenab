import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { ICarCategory } from 'app/shared/model/car/car-category.model';

type EntityResponseType = HttpResponse<ICarCategory>;
type EntityArrayResponseType = HttpResponse<ICarCategory[]>;

@Injectable({ providedIn: 'root' })
export class CarCategoryService {
  public resourceUrl = SERVER_API_URL + 'services/car/api/car-categories';
  public resourceSearchUrl = SERVER_API_URL + 'services/car/api/_search/car-categories';

  constructor(protected http: HttpClient) {}

  create(carCategory: ICarCategory): Observable<EntityResponseType> {
    return this.http.post<ICarCategory>(this.resourceUrl, carCategory, { observe: 'response' });
  }

  update(carCategory: ICarCategory): Observable<EntityResponseType> {
    return this.http.put<ICarCategory>(this.resourceUrl, carCategory, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICarCategory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICarCategory[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICarCategory[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}

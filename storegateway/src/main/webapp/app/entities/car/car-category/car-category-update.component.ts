import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { ICarCategory, CarCategory } from 'app/shared/model/car/car-category.model';
import { CarCategoryService } from './car-category.service';

@Component({
  selector: 'jhi-car-category-update',
  templateUrl: './car-category-update.component.html'
})
export class CarCategoryUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    description: []
  });

  constructor(protected carCategoryService: CarCategoryService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ carCategory }) => {
      this.updateForm(carCategory);
    });
  }

  updateForm(carCategory: ICarCategory): void {
    this.editForm.patchValue({
      id: carCategory.id,
      name: carCategory.name,
      description: carCategory.description
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const carCategory = this.createFromForm();
    if (carCategory.id !== undefined) {
      this.subscribeToSaveResponse(this.carCategoryService.update(carCategory));
    } else {
      this.subscribeToSaveResponse(this.carCategoryService.create(carCategory));
    }
  }

  private createFromForm(): ICarCategory {
    return {
      ...new CarCategory(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICarCategory>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { StoreSharedModule } from 'app/shared/shared.module';
import { CarCategoryComponent } from './car-category.component';
import { CarCategoryDetailComponent } from './car-category-detail.component';
import { CarCategoryUpdateComponent } from './car-category-update.component';
import { CarCategoryDeleteDialogComponent } from './car-category-delete-dialog.component';
import { carCategoryRoute } from './car-category.route';

@NgModule({
  imports: [StoreSharedModule, RouterModule.forChild(carCategoryRoute)],
  declarations: [CarCategoryComponent, CarCategoryDetailComponent, CarCategoryUpdateComponent, CarCategoryDeleteDialogComponent],
  entryComponents: [CarCategoryDeleteDialogComponent]
})
export class CarCarCategoryModule {}

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICarCategory, CarCategory } from 'app/shared/model/car/car-category.model';
import { CarCategoryService } from './car-category.service';
import { CarCategoryComponent } from './car-category.component';
import { CarCategoryDetailComponent } from './car-category-detail.component';
import { CarCategoryUpdateComponent } from './car-category-update.component';

@Injectable({ providedIn: 'root' })
export class CarCategoryResolve implements Resolve<ICarCategory> {
  constructor(private service: CarCategoryService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICarCategory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((carCategory: HttpResponse<CarCategory>) => {
          if (carCategory.body) {
            return of(carCategory.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CarCategory());
  }
}

export const carCategoryRoute: Routes = [
  {
    path: '',
    component: CarCategoryComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'storeApp.carCarCategory.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CarCategoryDetailComponent,
    resolve: {
      carCategory: CarCategoryResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'storeApp.carCarCategory.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CarCategoryUpdateComponent,
    resolve: {
      carCategory: CarCategoryResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'storeApp.carCarCategory.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CarCategoryUpdateComponent,
    resolve: {
      carCategory: CarCategoryResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'storeApp.carCarCategory.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

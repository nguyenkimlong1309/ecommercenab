import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICarCategory } from 'app/shared/model/car/car-category.model';
import { CarCategoryService } from './car-category.service';
import { CarCategoryDeleteDialogComponent } from './car-category-delete-dialog.component';

@Component({
  selector: 'jhi-car-category',
  templateUrl: './car-category.component.html'
})
export class CarCategoryComponent implements OnInit, OnDestroy {
  carCategories?: ICarCategory[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected carCategoryService: CarCategoryService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.carCategoryService
        .search({
          query: this.currentSearch
        })
        .subscribe((res: HttpResponse<ICarCategory[]>) => (this.carCategories = res.body || []));
      return;
    }

    this.carCategoryService.query().subscribe((res: HttpResponse<ICarCategory[]>) => (this.carCategories = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCarCategories();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICarCategory): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCarCategories(): void {
    this.eventSubscriber = this.eventManager.subscribe('carCategoryListModification', () => this.loadAll());
  }

  delete(carCategory: ICarCategory): void {
    const modalRef = this.modalService.open(CarCategoryDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.carCategory = carCategory;
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICarCategory } from 'app/shared/model/car/car-category.model';

@Component({
  selector: 'jhi-car-category-detail',
  templateUrl: './car-category-detail.component.html'
})
export class CarCategoryDetailComponent implements OnInit {
  carCategory: ICarCategory | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ carCategory }) => (this.carCategory = carCategory));
  }

  previousState(): void {
    window.history.back();
  }
}

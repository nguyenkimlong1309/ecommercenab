import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { ICustAct, CustAct } from 'app/shared/model/custAct/cust-act.model';
import { CustActService } from './cust-act.service';
import { CustActComponent } from './cust-act.component';
import { CustActDetailComponent } from './cust-act-detail.component';
import { CustActUpdateComponent } from './cust-act-update.component';

@Injectable({ providedIn: 'root' })
export class CustActResolve implements Resolve<ICustAct> {
  constructor(private service: CustActService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICustAct> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((custAct: HttpResponse<CustAct>) => {
          if (custAct.body) {
            return of(custAct.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CustAct());
  }
}

export const custActRoute: Routes = [
  {
    path: '',
    component: CustActComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'storeApp.custActCustAct.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CustActDetailComponent,
    resolve: {
      custAct: CustActResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'storeApp.custActCustAct.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CustActUpdateComponent,
    resolve: {
      custAct: CustActResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'storeApp.custActCustAct.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CustActUpdateComponent,
    resolve: {
      custAct: CustActResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'storeApp.custActCustAct.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

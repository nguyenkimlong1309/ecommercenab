import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { ICustAct, CustAct } from 'app/shared/model/custAct/cust-act.model';
import { CustActService } from './cust-act.service';

@Component({
  selector: 'jhi-cust-act-update',
  templateUrl: './cust-act-update.component.html'
})
export class CustActUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    date: [null, [Validators.required]],
    details: [],
    sentDate: [null, [Validators.required]],
    searchCount: [],
    viewCount: [],
    filterCount: [],
    carId: [null, [Validators.required]],
    ipAdd: []
  });

  constructor(protected custActService: CustActService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ custAct }) => {
      if (!custAct.id) {
        const today = moment().startOf('day');
        custAct.date = today;
        custAct.sentDate = today;
      }

      this.updateForm(custAct);
    });
  }

  updateForm(custAct: ICustAct): void {
    this.editForm.patchValue({
      id: custAct.id,
      date: custAct.date ? custAct.date.format(DATE_TIME_FORMAT) : null,
      details: custAct.details,
      sentDate: custAct.sentDate ? custAct.sentDate.format(DATE_TIME_FORMAT) : null,
      searchCount: custAct.searchCount,
      viewCount: custAct.viewCount,
      filterCount: custAct.filterCount,
      carId: custAct.carId,
      ipAdd: custAct.ipAdd
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const custAct = this.createFromForm();
    if (custAct.id !== undefined) {
      this.subscribeToSaveResponse(this.custActService.update(custAct));
    } else {
      this.subscribeToSaveResponse(this.custActService.create(custAct));
    }
  }

  private createFromForm(): ICustAct {
    return {
      ...new CustAct(),
      id: this.editForm.get(['id'])!.value,
      date: this.editForm.get(['date'])!.value ? moment(this.editForm.get(['date'])!.value, DATE_TIME_FORMAT) : undefined,
      details: this.editForm.get(['details'])!.value,
      sentDate: this.editForm.get(['sentDate'])!.value ? moment(this.editForm.get(['sentDate'])!.value, DATE_TIME_FORMAT) : undefined,
      searchCount: this.editForm.get(['searchCount'])!.value,
      viewCount: this.editForm.get(['viewCount'])!.value,
      filterCount: this.editForm.get(['filterCount'])!.value,
      carId: this.editForm.get(['carId'])!.value,
      ipAdd: this.editForm.get(['ipAdd'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICustAct>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}

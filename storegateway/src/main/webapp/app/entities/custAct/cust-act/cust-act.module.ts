import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { StoreSharedModule } from 'app/shared/shared.module';
import { CustActComponent } from './cust-act.component';
import { CustActDetailComponent } from './cust-act-detail.component';
import { CustActUpdateComponent } from './cust-act-update.component';
import { CustActDeleteDialogComponent } from './cust-act-delete-dialog.component';
import { custActRoute } from './cust-act.route';

@NgModule({
  imports: [StoreSharedModule, RouterModule.forChild(custActRoute)],
  declarations: [CustActComponent, CustActDetailComponent, CustActUpdateComponent, CustActDeleteDialogComponent],
  entryComponents: [CustActDeleteDialogComponent]
})
export class CustActCustActModule {}

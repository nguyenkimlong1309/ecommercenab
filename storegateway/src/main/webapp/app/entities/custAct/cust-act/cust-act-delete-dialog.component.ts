import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICustAct } from 'app/shared/model/custAct/cust-act.model';
import { CustActService } from './cust-act.service';

@Component({
  templateUrl: './cust-act-delete-dialog.component.html'
})
export class CustActDeleteDialogComponent {
  custAct?: ICustAct;

  constructor(protected custActService: CustActService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.custActService.delete(id).subscribe(() => {
      this.eventManager.broadcast('custActListModification');
      this.activeModal.close();
    });
  }
}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICustAct } from 'app/shared/model/custAct/cust-act.model';

@Component({
  selector: 'jhi-cust-act-detail',
  templateUrl: './cust-act-detail.component.html'
})
export class CustActDetailComponent implements OnInit {
  custAct: ICustAct | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ custAct }) => (this.custAct = custAct));
  }

  previousState(): void {
    window.history.back();
  }
}

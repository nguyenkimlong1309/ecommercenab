import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICustAct } from 'app/shared/model/custAct/cust-act.model';
import { CustActService } from './cust-act.service';
import { CustActDeleteDialogComponent } from './cust-act-delete-dialog.component';

@Component({
  selector: 'jhi-cust-act',
  templateUrl: './cust-act.component.html'
})
export class CustActComponent implements OnInit, OnDestroy {
  custActs?: ICustAct[];
  eventSubscriber?: Subscription;
  currentSearch: string;

  constructor(
    protected custActService: CustActService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected activatedRoute: ActivatedRoute
  ) {
    this.currentSearch =
      this.activatedRoute.snapshot && this.activatedRoute.snapshot.queryParams['search']
        ? this.activatedRoute.snapshot.queryParams['search']
        : '';
  }

  loadAll(): void {
    if (this.currentSearch) {
      this.custActService
        .search({
          query: this.currentSearch
        })
        .subscribe((res: HttpResponse<ICustAct[]>) => (this.custActs = res.body || []));
      return;
    }

    this.custActService.query().subscribe((res: HttpResponse<ICustAct[]>) => (this.custActs = res.body || []));
  }

  search(query: string): void {
    this.currentSearch = query;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInCustActs();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: ICustAct): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInCustActs(): void {
    this.eventSubscriber = this.eventManager.subscribe('custActListModification', () => this.loadAll());
  }

  delete(custAct: ICustAct): void {
    const modalRef = this.modalService.open(CustActDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.custAct = custAct;
  }
}

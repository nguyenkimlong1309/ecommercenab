import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, Search } from 'app/shared/util/request-util';
import { ICustAct } from 'app/shared/model/custAct/cust-act.model';

type EntityResponseType = HttpResponse<ICustAct>;
type EntityArrayResponseType = HttpResponse<ICustAct[]>;

@Injectable({ providedIn: 'root' })
export class CustActService {
  public resourceUrl = SERVER_API_URL + 'services/custact/api/cust-acts';
  public resourceSearchUrl = SERVER_API_URL + 'services/custact/api/_search/cust-acts';

  constructor(protected http: HttpClient) {}

  create(custAct: ICustAct): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(custAct);
    return this.http
      .post<ICustAct>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(custAct: ICustAct): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(custAct);
    return this.http
      .put<ICustAct>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICustAct>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICustAct[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: Search): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICustAct[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(custAct: ICustAct): ICustAct {
    const copy: ICustAct = Object.assign({}, custAct, {
      date: custAct.date && custAct.date.isValid() ? custAct.date.toJSON() : undefined,
      sentDate: custAct.sentDate && custAct.sentDate.isValid() ? custAct.sentDate.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? moment(res.body.date) : undefined;
      res.body.sentDate = res.body.sentDate ? moment(res.body.sentDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((custAct: ICustAct) => {
        custAct.date = custAct.date ? moment(custAct.date) : undefined;
        custAct.sentDate = custAct.sentDate ? moment(custAct.sentDate) : undefined;
      });
    }
    return res;
  }
}

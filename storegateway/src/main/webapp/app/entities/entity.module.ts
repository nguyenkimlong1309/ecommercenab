import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'car',
        loadChildren: () => import('./car/car/car.module').then(m => m.CarCarModule)
      },
      {
        path: 'car-category',
        loadChildren: () => import('./car/car-category/car-category.module').then(m => m.CarCarCategoryModule)
      },
      {
        path: 'cust-act',
        loadChildren: () => import('./custAct/cust-act/cust-act.module').then(m => m.CustActCustActModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class StoreEntityModule {}

/**
 * View Models used by Spring MVC REST controllers.
 */
package co.longuyen.store.gateway.web.rest.vm;

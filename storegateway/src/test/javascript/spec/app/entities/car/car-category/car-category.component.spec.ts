import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { StoreTestModule } from '../../../../test.module';
import { CarCategoryComponent } from 'app/entities/car/car-category/car-category.component';
import { CarCategoryService } from 'app/entities/car/car-category/car-category.service';
import { CarCategory } from 'app/shared/model/car/car-category.model';

describe('Component Tests', () => {
  describe('CarCategory Management Component', () => {
    let comp: CarCategoryComponent;
    let fixture: ComponentFixture<CarCategoryComponent>;
    let service: CarCategoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [StoreTestModule],
        declarations: [CarCategoryComponent]
      })
        .overrideTemplate(CarCategoryComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CarCategoryComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CarCategoryService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CarCategory(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.carCategories && comp.carCategories[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});

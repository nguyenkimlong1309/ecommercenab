import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { StoreTestModule } from '../../../../test.module';
import { CarCategoryUpdateComponent } from 'app/entities/car/car-category/car-category-update.component';
import { CarCategoryService } from 'app/entities/car/car-category/car-category.service';
import { CarCategory } from 'app/shared/model/car/car-category.model';

describe('Component Tests', () => {
  describe('CarCategory Management Update Component', () => {
    let comp: CarCategoryUpdateComponent;
    let fixture: ComponentFixture<CarCategoryUpdateComponent>;
    let service: CarCategoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [StoreTestModule],
        declarations: [CarCategoryUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CarCategoryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CarCategoryUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CarCategoryService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CarCategory(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CarCategory();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

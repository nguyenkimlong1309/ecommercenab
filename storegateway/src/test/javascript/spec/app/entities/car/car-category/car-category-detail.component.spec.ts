import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { StoreTestModule } from '../../../../test.module';
import { CarCategoryDetailComponent } from 'app/entities/car/car-category/car-category-detail.component';
import { CarCategory } from 'app/shared/model/car/car-category.model';

describe('Component Tests', () => {
  describe('CarCategory Management Detail Component', () => {
    let comp: CarCategoryDetailComponent;
    let fixture: ComponentFixture<CarCategoryDetailComponent>;
    const route = ({ data: of({ carCategory: new CarCategory(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [StoreTestModule],
        declarations: [CarCategoryDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CarCategoryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CarCategoryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load carCategory on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.carCategory).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { StoreTestModule } from '../../../../test.module';
import { CustActDetailComponent } from 'app/entities/custAct/cust-act/cust-act-detail.component';
import { CustAct } from 'app/shared/model/custAct/cust-act.model';

describe('Component Tests', () => {
  describe('CustAct Management Detail Component', () => {
    let comp: CustActDetailComponent;
    let fixture: ComponentFixture<CustActDetailComponent>;
    const route = ({ data: of({ custAct: new CustAct(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [StoreTestModule],
        declarations: [CustActDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CustActDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CustActDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load custAct on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.custAct).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { StoreTestModule } from '../../../../test.module';
import { CustActUpdateComponent } from 'app/entities/custAct/cust-act/cust-act-update.component';
import { CustActService } from 'app/entities/custAct/cust-act/cust-act.service';
import { CustAct } from 'app/shared/model/custAct/cust-act.model';

describe('Component Tests', () => {
  describe('CustAct Management Update Component', () => {
    let comp: CustActUpdateComponent;
    let fixture: ComponentFixture<CustActUpdateComponent>;
    let service: CustActService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [StoreTestModule],
        declarations: [CustActUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CustActUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CustActUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CustActService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CustAct(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CustAct();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

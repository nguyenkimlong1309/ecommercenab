import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { StoreTestModule } from '../../../../test.module';
import { CustActComponent } from 'app/entities/custAct/cust-act/cust-act.component';
import { CustActService } from 'app/entities/custAct/cust-act/cust-act.service';
import { CustAct } from 'app/shared/model/custAct/cust-act.model';

describe('Component Tests', () => {
  describe('CustAct Management Component', () => {
    let comp: CustActComponent;
    let fixture: ComponentFixture<CustActComponent>;
    let service: CustActService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [StoreTestModule],
        declarations: [CustActComponent]
      })
        .overrideTemplate(CustActComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CustActComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CustActService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new CustAct(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.custActs && comp.custActs[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});

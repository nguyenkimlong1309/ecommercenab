import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { CarComponentsPage, CarDeleteDialog, CarUpdatePage } from './car.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('Car e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let carComponentsPage: CarComponentsPage;
  let carUpdatePage: CarUpdatePage;
  let carDeleteDialog: CarDeleteDialog;
  const fileNameToUpload = 'logo-jhipster.png';
  const fileToUpload = '../../../../../../../src/main/webapp/content/images/' + fileNameToUpload;
  const absolutePath = path.resolve(__dirname, fileToUpload);

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Cars', async () => {
    await navBarPage.goToEntity('car');
    carComponentsPage = new CarComponentsPage();
    await browser.wait(ec.visibilityOf(carComponentsPage.title), 5000);
    expect(await carComponentsPage.getTitle()).to.eq('storeApp.carCar.home.title');
    await browser.wait(ec.or(ec.visibilityOf(carComponentsPage.entities), ec.visibilityOf(carComponentsPage.noResult)), 1000);
  });

  it('should load create Car page', async () => {
    await carComponentsPage.clickOnCreateButton();
    carUpdatePage = new CarUpdatePage();
    expect(await carUpdatePage.getPageTitle()).to.eq('storeApp.carCar.home.createOrEditLabel');
    await carUpdatePage.cancel();
  });

  it('should create and save Cars', async () => {
    const nbButtonsBeforeCreate = await carComponentsPage.countDeleteButtons();

    await carComponentsPage.clickOnCreateButton();

    await promise.all([
      carUpdatePage.setNameInput('name'),
      carUpdatePage.setDescriptionInput('description'),
      carUpdatePage.setPriceInput('5'),
      carUpdatePage.setBranchInput('branch'),
      carUpdatePage.colorSelectLastOption(),
      carUpdatePage.setImageInput(absolutePath),
      carUpdatePage.carCategorySelectLastOption()
    ]);

    expect(await carUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await carUpdatePage.getDescriptionInput()).to.eq('description', 'Expected Description value to be equals to description');
    expect(await carUpdatePage.getPriceInput()).to.eq('5', 'Expected price value to be equals to 5');
    expect(await carUpdatePage.getBranchInput()).to.eq('branch', 'Expected Branch value to be equals to branch');
    expect(await carUpdatePage.getImageInput()).to.endsWith(fileNameToUpload, 'Expected Image value to be end with ' + fileNameToUpload);

    await carUpdatePage.save();
    expect(await carUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await carComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Car', async () => {
    const nbButtonsBeforeDelete = await carComponentsPage.countDeleteButtons();
    await carComponentsPage.clickOnLastDeleteButton();

    carDeleteDialog = new CarDeleteDialog();
    expect(await carDeleteDialog.getDialogTitle()).to.eq('storeApp.carCar.delete.question');
    await carDeleteDialog.clickOnConfirmButton();

    expect(await carComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

import { element, by, ElementFinder } from 'protractor';

export class CarComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-car div table .btn-danger'));
  title = element.all(by.css('jhi-car div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CarUpdatePage {
  pageTitle = element(by.id('jhi-car-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  nameInput = element(by.id('field_name'));
  descriptionInput = element(by.id('field_description'));
  priceInput = element(by.id('field_price'));
  branchInput = element(by.id('field_branch'));
  colorSelect = element(by.id('field_color'));
  imageInput = element(by.id('file_image'));

  carCategorySelect = element(by.id('field_carCategory'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNameInput(name: string): Promise<void> {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput(): Promise<string> {
    return await this.nameInput.getAttribute('value');
  }

  async setDescriptionInput(description: string): Promise<void> {
    await this.descriptionInput.sendKeys(description);
  }

  async getDescriptionInput(): Promise<string> {
    return await this.descriptionInput.getAttribute('value');
  }

  async setPriceInput(price: string): Promise<void> {
    await this.priceInput.sendKeys(price);
  }

  async getPriceInput(): Promise<string> {
    return await this.priceInput.getAttribute('value');
  }

  async setBranchInput(branch: string): Promise<void> {
    await this.branchInput.sendKeys(branch);
  }

  async getBranchInput(): Promise<string> {
    return await this.branchInput.getAttribute('value');
  }

  async setColorSelect(color: string): Promise<void> {
    await this.colorSelect.sendKeys(color);
  }

  async getColorSelect(): Promise<string> {
    return await this.colorSelect.element(by.css('option:checked')).getText();
  }

  async colorSelectLastOption(): Promise<void> {
    await this.colorSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setImageInput(image: string): Promise<void> {
    await this.imageInput.sendKeys(image);
  }

  async getImageInput(): Promise<string> {
    return await this.imageInput.getAttribute('value');
  }

  async carCategorySelectLastOption(): Promise<void> {
    await this.carCategorySelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async carCategorySelectOption(option: string): Promise<void> {
    await this.carCategorySelect.sendKeys(option);
  }

  getCarCategorySelect(): ElementFinder {
    return this.carCategorySelect;
  }

  async getCarCategorySelectedOption(): Promise<string> {
    return await this.carCategorySelect.element(by.css('option:checked')).getText();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CarDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-car-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-car'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}

import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { CarCategoryComponentsPage, CarCategoryDeleteDialog, CarCategoryUpdatePage } from './car-category.page-object';

const expect = chai.expect;

describe('CarCategory e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let carCategoryComponentsPage: CarCategoryComponentsPage;
  let carCategoryUpdatePage: CarCategoryUpdatePage;
  let carCategoryDeleteDialog: CarCategoryDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load CarCategories', async () => {
    await navBarPage.goToEntity('car-category');
    carCategoryComponentsPage = new CarCategoryComponentsPage();
    await browser.wait(ec.visibilityOf(carCategoryComponentsPage.title), 5000);
    expect(await carCategoryComponentsPage.getTitle()).to.eq('storeApp.carCarCategory.home.title');
    await browser.wait(
      ec.or(ec.visibilityOf(carCategoryComponentsPage.entities), ec.visibilityOf(carCategoryComponentsPage.noResult)),
      1000
    );
  });

  it('should load create CarCategory page', async () => {
    await carCategoryComponentsPage.clickOnCreateButton();
    carCategoryUpdatePage = new CarCategoryUpdatePage();
    expect(await carCategoryUpdatePage.getPageTitle()).to.eq('storeApp.carCarCategory.home.createOrEditLabel');
    await carCategoryUpdatePage.cancel();
  });

  it('should create and save CarCategories', async () => {
    const nbButtonsBeforeCreate = await carCategoryComponentsPage.countDeleteButtons();

    await carCategoryComponentsPage.clickOnCreateButton();

    await promise.all([carCategoryUpdatePage.setNameInput('name'), carCategoryUpdatePage.setDescriptionInput('description')]);

    expect(await carCategoryUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await carCategoryUpdatePage.getDescriptionInput()).to.eq(
      'description',
      'Expected Description value to be equals to description'
    );

    await carCategoryUpdatePage.save();
    expect(await carCategoryUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await carCategoryComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last CarCategory', async () => {
    const nbButtonsBeforeDelete = await carCategoryComponentsPage.countDeleteButtons();
    await carCategoryComponentsPage.clickOnLastDeleteButton();

    carCategoryDeleteDialog = new CarCategoryDeleteDialog();
    expect(await carCategoryDeleteDialog.getDialogTitle()).to.eq('storeApp.carCarCategory.delete.question');
    await carCategoryDeleteDialog.clickOnConfirmButton();

    expect(await carCategoryComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

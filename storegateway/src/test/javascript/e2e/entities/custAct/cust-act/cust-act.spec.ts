import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../../page-objects/jhi-page-objects';

import { CustActComponentsPage, CustActDeleteDialog, CustActUpdatePage } from './cust-act.page-object';

const expect = chai.expect;

describe('CustAct e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let custActComponentsPage: CustActComponentsPage;
  let custActUpdatePage: CustActUpdatePage;
  let custActDeleteDialog: CustActDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load CustActs', async () => {
    await navBarPage.goToEntity('cust-act');
    custActComponentsPage = new CustActComponentsPage();
    await browser.wait(ec.visibilityOf(custActComponentsPage.title), 5000);
    expect(await custActComponentsPage.getTitle()).to.eq('storeApp.custActCustAct.home.title');
    await browser.wait(ec.or(ec.visibilityOf(custActComponentsPage.entities), ec.visibilityOf(custActComponentsPage.noResult)), 1000);
  });

  it('should load create CustAct page', async () => {
    await custActComponentsPage.clickOnCreateButton();
    custActUpdatePage = new CustActUpdatePage();
    expect(await custActUpdatePage.getPageTitle()).to.eq('storeApp.custActCustAct.home.createOrEditLabel');
    await custActUpdatePage.cancel();
  });

  it('should create and save CustActs', async () => {
    const nbButtonsBeforeCreate = await custActComponentsPage.countDeleteButtons();

    await custActComponentsPage.clickOnCreateButton();

    await promise.all([
      custActUpdatePage.setDateInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      custActUpdatePage.setDetailsInput('details'),
      custActUpdatePage.setSentDateInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
      custActUpdatePage.setSearchCountInput('5'),
      custActUpdatePage.setViewCountInput('5'),
      custActUpdatePage.setFilterCountInput('5'),
      custActUpdatePage.setCarIdInput('5'),
      custActUpdatePage.setIpAddInput('ipAdd')
    ]);

    expect(await custActUpdatePage.getDateInput()).to.contain('2001-01-01T02:30', 'Expected date value to be equals to 2000-12-31');
    expect(await custActUpdatePage.getDetailsInput()).to.eq('details', 'Expected Details value to be equals to details');
    expect(await custActUpdatePage.getSentDateInput()).to.contain('2001-01-01T02:30', 'Expected sentDate value to be equals to 2000-12-31');
    expect(await custActUpdatePage.getSearchCountInput()).to.eq('5', 'Expected searchCount value to be equals to 5');
    expect(await custActUpdatePage.getViewCountInput()).to.eq('5', 'Expected viewCount value to be equals to 5');
    expect(await custActUpdatePage.getFilterCountInput()).to.eq('5', 'Expected filterCount value to be equals to 5');
    expect(await custActUpdatePage.getCarIdInput()).to.eq('5', 'Expected carId value to be equals to 5');
    expect(await custActUpdatePage.getIpAddInput()).to.eq('ipAdd', 'Expected IpAdd value to be equals to ipAdd');

    await custActUpdatePage.save();
    expect(await custActUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await custActComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last CustAct', async () => {
    const nbButtonsBeforeDelete = await custActComponentsPage.countDeleteButtons();
    await custActComponentsPage.clickOnLastDeleteButton();

    custActDeleteDialog = new CustActDeleteDialog();
    expect(await custActDeleteDialog.getDialogTitle()).to.eq('storeApp.custActCustAct.delete.question');
    await custActDeleteDialog.clickOnConfirmButton();

    expect(await custActComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});

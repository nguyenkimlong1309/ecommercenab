import { element, by, ElementFinder } from 'protractor';

export class CustActComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-cust-act div table .btn-danger'));
  title = element.all(by.css('jhi-cust-act div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class CustActUpdatePage {
  pageTitle = element(by.id('jhi-cust-act-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  dateInput = element(by.id('field_date'));
  detailsInput = element(by.id('field_details'));
  sentDateInput = element(by.id('field_sentDate'));
  searchCountInput = element(by.id('field_searchCount'));
  viewCountInput = element(by.id('field_viewCount'));
  filterCountInput = element(by.id('field_filterCount'));
  carIdInput = element(by.id('field_carId'));
  ipAddInput = element(by.id('field_ipAdd'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setDateInput(date: string): Promise<void> {
    await this.dateInput.sendKeys(date);
  }

  async getDateInput(): Promise<string> {
    return await this.dateInput.getAttribute('value');
  }

  async setDetailsInput(details: string): Promise<void> {
    await this.detailsInput.sendKeys(details);
  }

  async getDetailsInput(): Promise<string> {
    return await this.detailsInput.getAttribute('value');
  }

  async setSentDateInput(sentDate: string): Promise<void> {
    await this.sentDateInput.sendKeys(sentDate);
  }

  async getSentDateInput(): Promise<string> {
    return await this.sentDateInput.getAttribute('value');
  }

  async setSearchCountInput(searchCount: string): Promise<void> {
    await this.searchCountInput.sendKeys(searchCount);
  }

  async getSearchCountInput(): Promise<string> {
    return await this.searchCountInput.getAttribute('value');
  }

  async setViewCountInput(viewCount: string): Promise<void> {
    await this.viewCountInput.sendKeys(viewCount);
  }

  async getViewCountInput(): Promise<string> {
    return await this.viewCountInput.getAttribute('value');
  }

  async setFilterCountInput(filterCount: string): Promise<void> {
    await this.filterCountInput.sendKeys(filterCount);
  }

  async getFilterCountInput(): Promise<string> {
    return await this.filterCountInput.getAttribute('value');
  }

  async setCarIdInput(carId: string): Promise<void> {
    await this.carIdInput.sendKeys(carId);
  }

  async getCarIdInput(): Promise<string> {
    return await this.carIdInput.getAttribute('value');
  }

  async setIpAddInput(ipAdd: string): Promise<void> {
    await this.ipAddInput.sendKeys(ipAdd);
  }

  async getIpAddInput(): Promise<string> {
    return await this.ipAddInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class CustActDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-custAct-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-custAct'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}

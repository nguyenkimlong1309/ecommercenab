# ecommerceNAB

# Please open README.md in store, car, custact directory for further detail.
### A high-level solution diagram for the components
![High Level Diagram](highlevelsolutiondiagram.png)
### Brief explanation for the code folder structure

- This is microservice architecture includes 2 microservices (Car service and CustAct service) and 1 gateway with front end and authentication jwt backend . They works each other via Jhipster Register service in Docker. Every micro services will be put in a docker.  
- The entities will be resided in domain package and support repository layer to talk with database and ElasticSearch layer.
dto package supports the service layer.
- All dependencies configuration will be resided in config directory.
- All the security relevance like authentication, authorization will be resided in security directory/package
- The web directory will contain files to support compose RESTful APIs.
- Test directory is for UT/IT .
- Liquibase directory is to track the database changes log. 
- Webapp directory is for angular 9 front end content. 
### All the required steps in order to get the application run on local computer
1. Start JHipster Registery docker first: docker-compose -f src/main/docker/jhipster-registry.yml up -d
2. npm install
3. Start gateway backend:
3.1 docker-compose -f src/main/docker/mysql.yml up -d 
3.2 ./mvnw
4. npm start
5. go to carstore directory: 
5.1 docker-compose -f src/main/docker/mysql.yml up -d  
5.2 ./mvnw
6. go to custAct directory:  
6.1. docker-compose -f src/main/docker/mongodb.yml up -d
6.2 ./mvnw

### Full CURL commands to verify the APIs (include full request endpoint, HTTP Headers and request payload if any)
* curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/services/car/api/cars?page=0&size=20&sort=id,asc
* curl -i -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://localhost:8080/services/car/api/cars/1
* curl -i -H "Accept: application/json" -H "Content-Type: application/json" \
  -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiYXV0aCI6IlJPTEVfVVNFUiIsImV4cCI6MTU5MDIyMjM5M30.3uSbD0chzbL7odl53UhoJuEabB4VsqSSUz8rNupGJ7ixf7Bh1IQEW-1RySkRGhFPwrr_QDZfhT65akj20Ut7Cw" -X POST \
  --data '{"date":"2020-04-22T17:00:00.000Z","details":"detail","sentDate":"2020-04-22T17:00:00.000Z","searchCount":1,"viewCount":1,"filterCount":1,"carId":1,"ipAdd":"127.0.0.1"}' \
   http://localhost:8080/services/custact/api/cust-acts
### UML Class Diagram 
![UML Class Diagram](jhipster-jdl.png)
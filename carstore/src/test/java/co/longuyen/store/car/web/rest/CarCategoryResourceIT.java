package co.longuyen.store.car.web.rest;

import co.longuyen.store.car.CarApp;
import co.longuyen.store.car.domain.CarCategory;
import co.longuyen.store.car.repository.CarCategoryRepository;
import co.longuyen.store.car.service.CarCategoryService;
import co.longuyen.store.car.service.dto.CarCategoryDTO;
import co.longuyen.store.car.service.mapper.CarCategoryMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link CarCategoryResource} REST controller.
 */
@SpringBootTest(classes = CarApp.class)

@AutoConfigureMockMvc
@WithMockUser
public class CarCategoryResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private CarCategoryRepository carCategoryRepository;

    @Autowired
    private CarCategoryMapper carCategoryMapper;

    @Autowired
    private CarCategoryService carCategoryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCarCategoryMockMvc;

    private CarCategory carCategory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CarCategory createEntity(EntityManager em) {
        CarCategory carCategory = new CarCategory()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return carCategory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CarCategory createUpdatedEntity(EntityManager em) {
        CarCategory carCategory = new CarCategory()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        return carCategory;
    }

    @BeforeEach
    public void initTest() {
        carCategory = createEntity(em);
    }

    @Test
    @Transactional
    public void createCarCategory() throws Exception {
        int databaseSizeBeforeCreate = carCategoryRepository.findAll().size();

        // Create the CarCategory
        CarCategoryDTO carCategoryDTO = carCategoryMapper.toDto(carCategory);
        restCarCategoryMockMvc.perform(post("/api/car-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(carCategoryDTO)))
            .andExpect(status().isCreated());

        // Validate the CarCategory in the database
        List<CarCategory> carCategoryList = carCategoryRepository.findAll();
        assertThat(carCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        CarCategory testCarCategory = carCategoryList.get(carCategoryList.size() - 1);
        assertThat(testCarCategory.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCarCategory.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createCarCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = carCategoryRepository.findAll().size();

        // Create the CarCategory with an existing ID
        carCategory.setId(1L);
        CarCategoryDTO carCategoryDTO = carCategoryMapper.toDto(carCategory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCarCategoryMockMvc.perform(post("/api/car-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(carCategoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CarCategory in the database
        List<CarCategory> carCategoryList = carCategoryRepository.findAll();
        assertThat(carCategoryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = carCategoryRepository.findAll().size();
        // set the field null
        carCategory.setName(null);

        // Create the CarCategory, which fails.
        CarCategoryDTO carCategoryDTO = carCategoryMapper.toDto(carCategory);

        restCarCategoryMockMvc.perform(post("/api/car-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(carCategoryDTO)))
            .andExpect(status().isBadRequest());

        List<CarCategory> carCategoryList = carCategoryRepository.findAll();
        assertThat(carCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCarCategories() throws Exception {
        // Initialize the database
        carCategoryRepository.saveAndFlush(carCategory);

        // Get all the carCategoryList
        restCarCategoryMockMvc.perform(get("/api/car-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(carCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getCarCategory() throws Exception {
        // Initialize the database
        carCategoryRepository.saveAndFlush(carCategory);

        // Get the carCategory
        restCarCategoryMockMvc.perform(get("/api/car-categories/{id}", carCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(carCategory.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

    @Test
    @Transactional
    public void getNonExistingCarCategory() throws Exception {
        // Get the carCategory
        restCarCategoryMockMvc.perform(get("/api/car-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCarCategory() throws Exception {
        // Initialize the database
        carCategoryRepository.saveAndFlush(carCategory);

        int databaseSizeBeforeUpdate = carCategoryRepository.findAll().size();

        // Update the carCategory
        CarCategory updatedCarCategory = carCategoryRepository.findById(carCategory.getId()).get();
        // Disconnect from session so that the updates on updatedCarCategory are not directly saved in db
        em.detach(updatedCarCategory);
        updatedCarCategory
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        CarCategoryDTO carCategoryDTO = carCategoryMapper.toDto(updatedCarCategory);

        restCarCategoryMockMvc.perform(put("/api/car-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(carCategoryDTO)))
            .andExpect(status().isOk());

        // Validate the CarCategory in the database
        List<CarCategory> carCategoryList = carCategoryRepository.findAll();
        assertThat(carCategoryList).hasSize(databaseSizeBeforeUpdate);
        CarCategory testCarCategory = carCategoryList.get(carCategoryList.size() - 1);
        assertThat(testCarCategory.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCarCategory.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingCarCategory() throws Exception {
        int databaseSizeBeforeUpdate = carCategoryRepository.findAll().size();

        // Create the CarCategory
        CarCategoryDTO carCategoryDTO = carCategoryMapper.toDto(carCategory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCarCategoryMockMvc.perform(put("/api/car-categories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(carCategoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CarCategory in the database
        List<CarCategory> carCategoryList = carCategoryRepository.findAll();
        assertThat(carCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCarCategory() throws Exception {
        // Initialize the database
        carCategoryRepository.saveAndFlush(carCategory);

        int databaseSizeBeforeDelete = carCategoryRepository.findAll().size();

        // Delete the carCategory
        restCarCategoryMockMvc.perform(delete("/api/car-categories/{id}", carCategory.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CarCategory> carCategoryList = carCategoryRepository.findAll();
        assertThat(carCategoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

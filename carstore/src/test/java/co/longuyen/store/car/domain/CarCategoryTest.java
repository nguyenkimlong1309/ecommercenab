package co.longuyen.store.car.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import co.longuyen.store.car.web.rest.TestUtil;

public class CarCategoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CarCategory.class);
        CarCategory carCategory1 = new CarCategory();
        carCategory1.setId(1L);
        CarCategory carCategory2 = new CarCategory();
        carCategory2.setId(carCategory1.getId());
        assertThat(carCategory1).isEqualTo(carCategory2);
        carCategory2.setId(2L);
        assertThat(carCategory1).isNotEqualTo(carCategory2);
        carCategory1.setId(null);
        assertThat(carCategory1).isNotEqualTo(carCategory2);
    }
}

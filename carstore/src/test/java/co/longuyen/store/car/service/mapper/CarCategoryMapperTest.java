package co.longuyen.store.car.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class CarCategoryMapperTest {

    private CarCategoryMapper carCategoryMapper;

    @BeforeEach
    public void setUp() {
        carCategoryMapper = new CarCategoryMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(carCategoryMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(carCategoryMapper.fromId(null)).isNull();
    }
}

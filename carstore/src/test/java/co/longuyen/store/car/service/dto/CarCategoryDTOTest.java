package co.longuyen.store.car.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import co.longuyen.store.car.web.rest.TestUtil;

public class CarCategoryDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CarCategoryDTO.class);
        CarCategoryDTO carCategoryDTO1 = new CarCategoryDTO();
        carCategoryDTO1.setId(1L);
        CarCategoryDTO carCategoryDTO2 = new CarCategoryDTO();
        assertThat(carCategoryDTO1).isNotEqualTo(carCategoryDTO2);
        carCategoryDTO2.setId(carCategoryDTO1.getId());
        assertThat(carCategoryDTO1).isEqualTo(carCategoryDTO2);
        carCategoryDTO2.setId(2L);
        assertThat(carCategoryDTO1).isNotEqualTo(carCategoryDTO2);
        carCategoryDTO1.setId(null);
        assertThat(carCategoryDTO1).isNotEqualTo(carCategoryDTO2);
    }
}

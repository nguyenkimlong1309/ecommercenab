/**
 * View Models used by Spring MVC REST controllers.
 */
package co.longuyen.store.car.web.rest.vm;

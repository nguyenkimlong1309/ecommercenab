package co.longuyen.store.car.web.rest;

import co.longuyen.store.car.service.CarCategoryService;
import co.longuyen.store.car.web.rest.errors.BadRequestAlertException;
import co.longuyen.store.car.service.dto.CarCategoryDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link co.longuyen.store.car.domain.CarCategory}.
 */
@RestController
@RequestMapping("/api")
public class CarCategoryResource {

    private final Logger log = LoggerFactory.getLogger(CarCategoryResource.class);

    private static final String ENTITY_NAME = "carCarCategory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CarCategoryService carCategoryService;

    public CarCategoryResource(CarCategoryService carCategoryService) {
        this.carCategoryService = carCategoryService;
    }

    /**
     * {@code POST  /car-categories} : Create a new carCategory.
     *
     * @param carCategoryDTO the carCategoryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new carCategoryDTO, or with status {@code 400 (Bad Request)} if the carCategory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/car-categories")
    public ResponseEntity<CarCategoryDTO> createCarCategory(@Valid @RequestBody CarCategoryDTO carCategoryDTO) throws URISyntaxException {
        log.debug("REST request to save CarCategory : {}", carCategoryDTO);
        if (carCategoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new carCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CarCategoryDTO result = carCategoryService.save(carCategoryDTO);
        return ResponseEntity.created(new URI("/api/car-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /car-categories} : Updates an existing carCategory.
     *
     * @param carCategoryDTO the carCategoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated carCategoryDTO,
     * or with status {@code 400 (Bad Request)} if the carCategoryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the carCategoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/car-categories")
    public ResponseEntity<CarCategoryDTO> updateCarCategory(@Valid @RequestBody CarCategoryDTO carCategoryDTO) throws URISyntaxException {
        log.debug("REST request to update CarCategory : {}", carCategoryDTO);
        if (carCategoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CarCategoryDTO result = carCategoryService.save(carCategoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, carCategoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /car-categories} : get all the carCategories.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of carCategories in body.
     */
    @GetMapping("/car-categories")
    public List<CarCategoryDTO> getAllCarCategories() {
        log.debug("REST request to get all CarCategories");
        return carCategoryService.findAll();
    }

    /**
     * {@code GET  /car-categories/:id} : get the "id" carCategory.
     *
     * @param id the id of the carCategoryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the carCategoryDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/car-categories/{id}")
    public ResponseEntity<CarCategoryDTO> getCarCategory(@PathVariable Long id) {
        log.debug("REST request to get CarCategory : {}", id);
        Optional<CarCategoryDTO> carCategoryDTO = carCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(carCategoryDTO);
    }

    /**
     * {@code DELETE  /car-categories/:id} : delete the "id" carCategory.
     *
     * @param id the id of the carCategoryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/car-categories/{id}")
    public ResponseEntity<Void> deleteCarCategory(@PathVariable Long id) {
        log.debug("REST request to delete CarCategory : {}", id);
        carCategoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

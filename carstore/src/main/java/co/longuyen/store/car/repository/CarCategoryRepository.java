package co.longuyen.store.car.repository;

import co.longuyen.store.car.domain.CarCategory;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the CarCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CarCategoryRepository extends JpaRepository<CarCategory, Long> {
}

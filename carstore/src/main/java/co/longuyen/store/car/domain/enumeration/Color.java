package co.longuyen.store.car.domain.enumeration;

/**
 * The Color enumeration.
 */
public enum Color {
    RED, WHITE, YELLOW, BLACK, GREEN
}

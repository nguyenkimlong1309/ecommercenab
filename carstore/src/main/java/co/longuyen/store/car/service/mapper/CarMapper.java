package co.longuyen.store.car.service.mapper;


import co.longuyen.store.car.domain.*;
import co.longuyen.store.car.service.dto.CarDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Car} and its DTO {@link CarDTO}.
 */
@Mapper(componentModel = "spring", uses = {CarCategoryMapper.class})
public interface CarMapper extends EntityMapper<CarDTO, Car> {

    @Mapping(source = "carCategory.id", target = "carCategoryId")
    @Mapping(source = "carCategory.name", target = "carCategoryName")
    CarDTO toDto(Car car);

    @Mapping(source = "carCategoryId", target = "carCategory")
    Car toEntity(CarDTO carDTO);

    default Car fromId(Long id) {
        if (id == null) {
            return null;
        }
        Car car = new Car();
        car.setId(id);
        return car;
    }
}

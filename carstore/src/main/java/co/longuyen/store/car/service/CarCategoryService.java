package co.longuyen.store.car.service;

import co.longuyen.store.car.domain.CarCategory;
import co.longuyen.store.car.repository.CarCategoryRepository;
import co.longuyen.store.car.service.dto.CarCategoryDTO;
import co.longuyen.store.car.service.mapper.CarCategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link CarCategory}.
 */
@Service
@Transactional
public class CarCategoryService {

    private final Logger log = LoggerFactory.getLogger(CarCategoryService.class);

    private final CarCategoryRepository carCategoryRepository;

    private final CarCategoryMapper carCategoryMapper;

    public CarCategoryService(CarCategoryRepository carCategoryRepository, CarCategoryMapper carCategoryMapper) {
        this.carCategoryRepository = carCategoryRepository;
        this.carCategoryMapper = carCategoryMapper;
    }

    /**
     * Save a carCategory.
     *
     * @param carCategoryDTO the entity to save.
     * @return the persisted entity.
     */
    public CarCategoryDTO save(CarCategoryDTO carCategoryDTO) {
        log.debug("Request to save CarCategory : {}", carCategoryDTO);
        CarCategory carCategory = carCategoryMapper.toEntity(carCategoryDTO);
        carCategory = carCategoryRepository.save(carCategory);
        return carCategoryMapper.toDto(carCategory);
    }

    /**
     * Get all the carCategories.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CarCategoryDTO> findAll() {
        log.debug("Request to get all CarCategories");
        return carCategoryRepository.findAll().stream()
            .map(carCategoryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one carCategory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CarCategoryDTO> findOne(Long id) {
        log.debug("Request to get CarCategory : {}", id);
        return carCategoryRepository.findById(id)
            .map(carCategoryMapper::toDto);
    }

    /**
     * Delete the carCategory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete CarCategory : {}", id);
        carCategoryRepository.deleteById(id);
    }
}

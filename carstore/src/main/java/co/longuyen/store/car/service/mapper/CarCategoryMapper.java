package co.longuyen.store.car.service.mapper;


import co.longuyen.store.car.domain.*;
import co.longuyen.store.car.service.dto.CarCategoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link CarCategory} and its DTO {@link CarCategoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CarCategoryMapper extends EntityMapper<CarCategoryDTO, CarCategory> {


    @Mapping(target = "cars", ignore = true)
    @Mapping(target = "removeCar", ignore = true)
    CarCategory toEntity(CarCategoryDTO carCategoryDTO);

    default CarCategory fromId(Long id) {
        if (id == null) {
            return null;
        }
        CarCategory carCategory = new CarCategory();
        carCategory.setId(id);
        return carCategory;
    }
}
